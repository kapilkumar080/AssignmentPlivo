package com.plivo.services;

import com.jayway.restassured.response.Response;
import com.plivo.Base.Base;
import com.plivo.Utils.ConfigUtil;
import com.plivo.Utils.LoggerUtils;
import com.plivo.Utils.PlivoConstants;
import com.plivo.helpers.ApiHelpers;
import com.plivo.models.responsemodels.NumberDAO;

import java.util.HashMap;

public class NumberData extends Base {


    ApiHelpers restApiCall = new ApiHelpers();


    public NumberDAO getNumberData(String number){

        LoggerUtils.info("Calling Account Number API for Attributes of Number-"  );
        String url = baseUrl+baseEndPoint+AUTH_ID+ PlivoConstants.NUMBER+number;
        HashMap<String, Object> headers = new HashMap<String, Object>();
        HashMap<String, Object> body = new HashMap<String, Object>();
        HashMap<String, Object> qParams = new HashMap<String, Object>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", authorizationHeaderValue);
        Response res =  restApiCall.response_from_api("get", url, null, headers, null);
        NumberDAO numberDAO = ApiHelpers.get_Gson_from_Json(NumberDAO.class,res);
        LoggerUtils.info("response recieved-"+res.body().print());
        return numberDAO;
    }



}
