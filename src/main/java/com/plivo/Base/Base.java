package com.plivo.Base;

import com.plivo.Utils.ConfigUtil;
import com.plivo.Utils.PlivoConstants;

public class Base {





  //  public static String environmentName = System.getProperty("environment");


    public static String baseUrl = (ConfigUtil.getDataFromConfig("base_url")==null)?PlivoConstants.DEFAULT_URL:ConfigUtil.getDataFromConfig("base_url");
    public static String baseEndPoint = PlivoConstants.BASE_END_POINT;;

    public static String AUTH_ID = ConfigUtil.getDataFromConfig("Username");

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl() {
       String baseUrl = ConfigUtil.getDataFromConfig("base_url");
       if(baseUrl==null)
           baseUrl = PlivoConstants.DEFAULT_URL;
    }

    public String getBaseEndPoint() {
        return baseEndPoint;
    }

    public void setBaseEndPoint(String baseEndPoint) {


        this.baseEndPoint = PlivoConstants.BASE_END_POINT;

    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsernameAndPassword() {
        return usernameAndPassword;
    }

    public void setUsernameAndPassword(String usernameAndPassword) {
        this.usernameAndPassword = usernameAndPassword;
    }

    public String getAuthorizationHeaderValue() {
        return authorizationHeaderValue;
    }

    public void setAuthorizationHeaderValue(String authorizationHeaderValue) {
        this.authorizationHeaderValue = authorizationHeaderValue;
    }

    String username = ConfigUtil.getDataFromConfig("Username");
    String password = ConfigUtil.getDataFromConfig("Password");

    String usernameAndPassword = username + ":" + password;
    public   String authorizationHeaderValue = "Basic " + java.util.Base64.getEncoder().encodeToString( usernameAndPassword.getBytes() );

}
