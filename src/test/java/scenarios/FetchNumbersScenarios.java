package scenarios;

import com.jayway.restassured.response.Response;
import com.plivo.Utils.CommonUtil;
import com.plivo.Utils.LoggerUtils;
import com.plivo.helpers.ApiHelpers;
import com.plivo.models.responsemodels.AllNumbersDAO;
import com.plivo.models.responsemodels.ObjectsDAO;
import com.plivo.services.FetchNumbers;
import org.testng.Assert;


public class FetchNumbersScenarios extends IScenarios {

     CommonUtil commonUtil = new CommonUtil();
    private Response response;

    public FetchNumbersScenarios() {
    }

    public void execute() {
        FetchNumbers fetchNumbers = new FetchNumbers();
        response = fetchNumbers.getNumbers();
    }

    public void verify() {
        if (response.statusCode() != 200) {
            Assert.fail("Success Response is not recived from API");
        }
        AllNumbersDAO numbers = ApiHelpers.get_Gson_from_Json(AllNumbersDAO.class, response);
        Assert.assertNotNull(numbers, "response recived from numbers api is null");
        ObjectsDAO[] objs = numbers.getObjects();
        String phonenumber1 = objs[0].getNumber();
        String phonenumber2 = objs[1].getNumber();
        Assert.assertNotNull(phonenumber1, "Phone number is not found");
        Assert.assertNotNull(phonenumber2, "phone number is not found");
        commonUtil.saveNumbers(phonenumber1, phonenumber2);
        LoggerUtils.info("Numbers recived from Number API is "+phonenumber1+"   "+phonenumber2);
        new SendMessageScenarios(commonUtil);
    }
}
