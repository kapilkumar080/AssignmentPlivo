package scenarios;

import com.jayway.restassured.response.Response;
import com.plivo.Utils.CommonUtil;
import com.plivo.Utils.LoggerUtils;
import com.plivo.helpers.ApiHelpers;
import com.plivo.models.responsemodels.AccountDetailsDAO;
import com.plivo.services.AccountDetails;
import org.testng.Assert;

import java.text.DecimalFormat;

public class AccountDetailsScenarios extends IScenarios {

    private CommonUtil commonUtil = new CommonUtil();
    private Response response;

     AccountDetailsScenarios(CommonUtil commonUtil) {
        this.commonUtil = commonUtil;
        this.run();
    }

    @Override
    public void execute() {
        AccountDetails accountDetails = new AccountDetails();
        response = accountDetails.getAccountDetailsofPlivoAccount();
    }

    @Override
    public void verify() {
        if (response.statusCode() == 200) {
            System.out.println("***your plivo account details has been fetched***");
        } else
            Assert.fail("Account detail api is failing");
        AccountDetailsDAO accountDetailsDAO = ApiHelpers.get_Gson_from_Json(AccountDetailsDAO.class, response);
        String creditafterDeduction = accountDetailsDAO.getCash_credits();
        LoggerUtils.info("Credit amount of your plivo Account After sending message  "+creditafterDeduction);
        String creditBeforeDeduction = commonUtil.data.get("current_cash_credit");
        LoggerUtils.info("Credit amount of your plivo Account Before sending message  "+creditBeforeDeduction);
        float difference;
        difference = Float.parseFloat(creditBeforeDeduction) - Float.parseFloat(creditafterDeduction);
        DecimalFormat twoDForm = new DecimalFormat("#.#####");
        double expectedRateAmount = Double.valueOf(twoDForm.format(difference));
        LoggerUtils.info("Difference of Current credit amount and After sending message  "+expectedRateAmount);
        Assert.assertTrue(expectedRateAmount > 0);
        Float total_rate = Float.parseFloat(commonUtil.data.get("total_rate"));
        Double actualRate = Double.valueOf(twoDForm.format(total_rate));
        LoggerUtils.info("Rate charged for sending messgae based Country ISO  "+actualRate);
        Assert.assertEquals(expectedRateAmount,actualRate);
    }
}
