package testcases;


import com.plivo.Utils.LoggerUtils;
import org.testng.annotations.Test;
import scenarios.FetchNumbersScenarios;

public class PlivoTestCase {


    @Test
    public void verifyDeductedAmountAfterMessageSent( ) {

        LoggerUtils.info("Starting Test Case for Amount Deducted verification");
        LoggerUtils.info("<==================== Executing Test Method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() + " ====================>");
        FetchNumbersScenarios fetchNumbersScenarios = new FetchNumbersScenarios( );
        fetchNumbersScenarios.run();
    }


}
